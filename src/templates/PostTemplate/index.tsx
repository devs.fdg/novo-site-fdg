import { FacebookShareButton, FacebookIcon } from 'next-share'
import { PostTemplateProps } from 'types'
import Layout from 'components/Layout'
import { FDG_SITE_URL } from 'utils'
import { PageWrapper, StyledDescription } from './styles'

export default function PostTemplate({
  title,
  slug,
  description,
  publishedAt
}: PostTemplateProps) {
  return (
    <Layout>
      <PageWrapper>
        <h1>{title}</h1>
        {publishedAt && <time>{publishedAt}</time>}
        <FacebookShareButton url={`${FDG_SITE_URL}/${slug}`}>
          <FacebookIcon size={24} round />
        </FacebookShareButton>
        <StyledDescription dangerouslySetInnerHTML={{ __html: description }} />
      </PageWrapper>
    </Layout>
  )
}
