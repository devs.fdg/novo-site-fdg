import styled from 'styled-components'

export const PageWrapper = styled.section`
  padding: 24px;
  background-color: white;
  border-left: 20px solid #ccc;
`

export const StyledDescription = styled.div`
  font-size: var(--small);
  margin: 16px 0;

  p {
    margin: 2rem 0;
  }
`
