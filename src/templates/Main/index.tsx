import { NextSeo } from 'next-seo'
import { Posts } from 'types'
import { FDG_SITE_URL } from 'utils'
import Layout from 'components/Layout'
import { Wrapper } from './styles'

const Main = ({ posts }: Posts) => {
  return (
    <>
      <NextSeo
        title="Flamengo da Gente"
        description="Site oficial do grupo Flamengo da Gente"
        canonical={FDG_SITE_URL}
        openGraph={{
          url: FDG_SITE_URL,
          title: 'Flamengo da Gente',
          description: 'Site oficial do grupo Flamengo da Gente',
          images: [
            {
              url: '/img/logo_email_fdg.png',
              alt: 'Logo do Flamengo da Gente, com Flamengo escrito em vermelho sob da gente escrito em preto'
            }
          ]
        }}
      />
      <Layout>
        <Wrapper>
          <h1>Flamengo da Gente</h1>

          {posts.map((post, index) => (
            <div
              key={index}
              dangerouslySetInnerHTML={{ __html: post.description.html }}
            />
          ))}
        </Wrapper>
      </Layout>
    </>
  )
}

export default Main
