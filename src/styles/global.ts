import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: "OfficinaSerif";
    src: url("/fonts/OfficinaSerif-Regular.ttf") format("truetype") /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
    font-style: normal;
    font-weight: 400;
    font-display: swap;
  }

  @font-face {
    font-family: "OfficinaSerif";
    src: url("/fonts/OfficinaSerif-Bold.eot"); /* IE9*/
    src: url("/fonts/OfficinaSerif-Bold.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
    url("/fonts/OfficinaSerif-Bold.woff2") format("woff2"), /* chrome、firefox */
    url("/fonts/OfficinaSerif-Bold.woff") format("woff"), /* chrome、firefox */
    url("/fonts/OfficinaSerif-Bold.ttf") format("truetype") /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
    font-style: bold;
    font-weight: 700;
    font-display: swap;
  }

  :root {
    --red: #be1e32;
    --background: #eaebe9;
    --white: #eeeeee;
    --black: #363636;

    --container: 100rem;

    --xsmall: 1.25rem;
    --small: 1.5rem;
    --medium: 3rem;
    --large: 5rem;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%; // to calculate rem
  }

  html, body, #__next {
    height: 100%;
  }

  body {
    font-family: "Officina Serif", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, 
    "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    background: var(--background) url("/img/bg_artboard_02.jpg") repeat;
    background-size: cover;
  }
`

export default GlobalStyle
