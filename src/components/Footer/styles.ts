import styled from 'styled-components'

export const StyledFooter = styled.footer`
  display: flex;
  background-color: #ffffff;
  width: 100%;
  padding: 24px;
  flex-direction: column;
  align-content: space-between;
  align-items: center;

  @media (min-width: 768px) {
    flex-direction: row;
  }
`

export const FdG = styled.p`
  color: #545151;
  font-weight: 400;
  text-align: center;
  margin-top: 8px;
`

export const Logo = styled.div`
  flex: 1;
  text-align: center;
  margin-top: 24px;

  @media (min-width: 768px) {
    margin-top: 0;
  }
`
export const Description = styled.div`
  flex: 2;
  margin: 24px 0;

  p {
    font-size: var(--xsmall);
    max-width: 800px;
  }

  @media (min-width: 768px) {
    margin: 0;
  }
`

export const SocialMedia = styled.div`
  flex: 1;
  margin-bottom: 24px;

  ul {
    list-style: none;
  }

  li {
    margin-bottom: 4px;
  }

  a {
    text-decoration: none;
    font-size: 1.12rem;
    font-weight: 500;
    color: var(--black);
    margin-left: 4px;
  }

  a:hover {
    color: var(--red);
  }

  @media (min-width: 768px) {
    margin-bottom: 0;
  }
`
