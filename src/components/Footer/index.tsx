import Image from 'next/image'
import { Description, FdG, Logo, SocialMedia, StyledFooter } from './styles'
import { getYear } from 'utils'
import {
  InstagramWithCircle,
  TwitterWithCircle,
  YoutubeWithCircle
} from '@styled-icons/entypo-social'

const Footer = () => (
  <>
    <StyledFooter>
      <SocialMedia>
        <ul>
          {/* <li>
            <a href="facebook">FlamengoDaGente</a>
          </li> */}
          <li>
            <i>
              <TwitterWithCircle size={24} />
            </i>
            <a
              href="https://twitter.com/flamengodagente"
              target="_blank"
              rel="noreferrer"
            >
              FlamengodaGente
            </a>
          </li>
          <li>
            <i>
              <InstagramWithCircle size={24} />
            </i>
            <a
              href="https://www.instagram.com/flamengodagente/"
              target="_blank"
              rel="noreferrer"
            >
              FlamengodaGente
            </a>
          </li>
          <li>
            <i>
              <YoutubeWithCircle size={24} />
            </i>
            <a
              href="https://www.youtube.com/c/FlamengodaGente"
              target="_blank"
              rel="noreferrer"
            >
              FlamengodaGente
            </a>
          </li>
        </ul>
      </SocialMedia>

      <Description>
        <p>
          O Flamengo da Gente, movimento de sócios, sócios-torcedores e
          torcedores do Clube de Regatas do Flamengo, está construindo uma
          plataforma para dialogar com os todes que se identificam com as nossas
          pautas. Em breve, mais novidades por aqui.
        </p>
      </Description>

      <Logo>
        <Image
          src="/img/logo_fdg_redondo.png"
          alt="Logo com FdG escrito ao centro e, em formato de círculo, as palavras Flamengo da Gente, justo, democrático e popular"
          width={100}
          height={100}
        />
      </Logo>
    </StyledFooter>

    <FdG>
      <span>&copy;</span>Copyright - {getYear()} Flamengo da Gente
    </FdG>
  </>
)

export default Footer
