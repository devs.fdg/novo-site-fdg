import { GlobalLayout } from './styles'
import Footer from 'components/Footer'
import Header from 'components/Header'

const Layout = (props: any) => (
  <GlobalLayout>
    <Header />
    <main id="main">{props.children}</main>
    <Footer />
  </GlobalLayout>
)

export default Layout
