import styled from 'styled-components'

export const StyledHeader = styled.header`
  min-height: 100px;
  background-color: white;
  padding: 1.5rem;
`

export const StyledNav = styled.nav`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const NavLinksContainer = styled.div``

export const NavItems = styled.ul`
  display: flex;
  flex-direction: row;
  list-style: none;
  align-items: center;

  li {
    font-size: var(--small);
    margin: 0 8px;
  }
`

export const NavLink = styled.a`
  text-decoration: none;
  color: var(--black);
  font-weight: 600;
  cursor: pointer;

  &:hover {
    color: var(--red);
  }
`

export const FacaParteLink = styled.a`
  text-decoration: none;
  background-color: var(--red);
  padding: 1.5rem;
  color: white;
  border: none;
  font-size: var(--small);
  font-weight: 600;
  border-radius: 8px;
  cursor: pointer;

  &:hover {
    background-color: var(--black);
    color: white;
  }
`
