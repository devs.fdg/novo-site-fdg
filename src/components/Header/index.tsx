import Image from 'next/image'
import Link from 'next/link'
import {
  FacaParteLink,
  NavItems,
  NavLink,
  NavLinksContainer,
  StyledHeader,
  StyledNav
} from './styles'

const Header = () => (
  <StyledHeader>
    <StyledNav>
      <Link href="/">
        <a>
          <Image
            src="/img/logo_fdg.png"
            alt="Logo com FdG escrito na parte superior Flamengo em vermelho, e abaixo da gente em preto"
            width={120}
            height={70}
          />
        </a>
      </Link>

      <NavLinksContainer>
        <NavItems>
          <li>
            <Link href="quem-somos">
              <NavLink>Quem Somos</NavLink>
            </Link>
          </li>
          <li>
            <Link href="manifesto">
              <NavLink>Manifesto</NavLink>
            </Link>
          </li>
          <li>
            {/* need to add a dropdown with link to each group */}
            <NavLink>Frentes e Grupos de Trabalho</NavLink>
          </li>
          <li>
            <FacaParteLink href="">Faça Parte</FacaParteLink>
          </li>
        </NavItems>
      </NavLinksContainer>
    </StyledNav>
  </StyledHeader>
)
export default Header
