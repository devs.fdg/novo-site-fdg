import { Asset, Post } from 'graphql/generated/graphql'

export type Posts = {
  posts: Post[]
}

export type PostTemplateProps = {
  id?: string
  title: string
  slug: string
  description: string
  publishedAt: string
  gallery?: Asset[]
}
