export const getYear = () => {
  const date = new Date()
  return date.getFullYear()
}

export const FDG_SITE_URL = 'https://www.flamengodagente.com.br'
