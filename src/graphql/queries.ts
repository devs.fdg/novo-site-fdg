import { gql } from 'graphql-request'

export const GET_POSTS = gql`
  query getPosts {
    posts {
      id
      slug
      title
      category
      description {
        html
      }
    }
  }
`

export const GET_POSTS_BY_CATEGORY = gql`
  query getPostsByCategory($category: Categories) {
    posts(where: { category: $category }) {
      id
      slug
      title
      category
      description {
        html
      }
      gallery {
        url
        height
        width
      }
    }
  }
`

export const GET_POST_BY_SLUG = gql`
  query getPostBySlug($slug: String!) {
    post(where: { slug: $slug }) {
      id
      slug
      title
      category
      publishedAt
      description {
        html
      }
      gallery {
        url
        height
        width
      }
    }
  }
`

export const GET_MANIFESTO_POST = gql`
  query getManifestoPost {
    post(where: { slug: "manifesto" }) {
      id
      title
      slug
      publishedAt
      description {
        html
      }
    }
  }
`

export const GET_QUEM_SOMOS_POST = gql`
  query getQuemSomosPost {
    post(where: { slug: "quem-somos" }) {
      id
      title
      slug
      publishedAt
      description {
        html
      }
    }
  }
`
