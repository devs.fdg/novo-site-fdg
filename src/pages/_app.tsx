import type { AppProps } from 'next/app'
import Head from 'next/head'

import GlobalStyles from 'styles/global'

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Flamengo da Gente</title>
        <link rel="shortcut icon" href="/img/android-chrome-512x512.png" />
        <link rel="apple-touch-icon" href="/img/android-chrome-512x512.png" />
        <link rel="manifest" href="/manifest.json" />
        <link
          href="/fonts/OfficinaSerif-Regular.ttf"
          as="font"
          crossOrigin=""
        />
        <link href="/fonts/OfficinaSerif-Bold.ttf" as="font" crossOrigin="" />
        <meta name="theme-color" content="#eaebe9" />
        <meta name="description" content="Site do grupo Flamengo da Gente" />
      </Head>
      <GlobalStyles />
      <Component {...pageProps} />
    </>
  )
}

export default App
