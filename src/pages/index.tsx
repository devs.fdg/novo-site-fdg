import client from 'graphql/client'
import { GET_POSTS } from 'graphql/queries'
import { GetPostsQuery } from 'graphql/generated/graphql'
import { Posts } from 'types'
import Main from 'templates/Main'

export default function Home({ posts }: Posts) {
  return <Main posts={posts} />
}

export const getStaticProps = async () => {
  const { posts } = await client.request<GetPostsQuery>(GET_POSTS)

  return {
    revalidate: 5,
    props: {
      posts
    }
  }
}
