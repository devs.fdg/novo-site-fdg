import client from 'graphql/client'
import { GET_MANIFESTO_POST } from 'graphql/queries'
import { GetManifestoPostQuery } from 'graphql/generated/graphql'
import { PostTemplateProps } from 'types'
import PostTemplate from 'templates/PostTemplate'

export default function Manifesto({
  title,
  slug,
  description,
  publishedAt
}: PostTemplateProps) {
  return (
    <PostTemplate
      title={title}
      slug={slug}
      description={description}
      publishedAt={publishedAt}
    />
  )
}

export const getStaticProps = async () => {
  const { post } = await client.request<GetManifestoPostQuery>(
    GET_MANIFESTO_POST
  )

  if (!post) return { notFound: true }

  return {
    props: {
      title: post.title,
      slug: post.slug,
      description: post.description.html,
      publishedAt: post.publishedAt
    }
  }
}
