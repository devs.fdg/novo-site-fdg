export default {
  openGraph: {
    type: 'website',
    locale: 'pt_BR',
    url: 'https://www.flamengodagente.com.br',
    site_name: 'Flamengo da Gente'
  },
  twitter: {
    handle: '@flamengodagente',
    site: 'https://www.flamengodagente.com.br',
    cardType: 'summary_large_image'
  }
}
