# Site - Flamengo da Gente
Repositório do novo site do Flamengo da Gente

## Stack
- Typescript
- Next.js
- GraphQL, usando o GraphCMS como gerenciador de conteúdo
- Styled Components

## Scripts
Para rodar o projeto localmente, use o seguinte comando: `yarn dev`

O comando `yarn build` cria arquivos que podem ser publicados em prod/staging. Para rodar a versão prod, depois do build, use o comando `yarn start`

Além dos comandos para executar a aplicação, temos também comandos para rodar a suite de testes e para formatação do código de acordo com os padrões definidos nos arquivos `.babelrc`, `.prettierrc`

## Variáveis de ambiente

## Acesso aos repositórios
